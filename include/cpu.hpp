#ifndef CPU_HPP
#define CPU_HPP

#include "register.hpp"
#include "adder.hpp"
#include "memory.hpp"
#include "splitter.hpp"
#include "control.hpp"
#include "register_file.hpp"
//#include "alu.hpp"
#include "ALU_RTL.hpp"
#include "extend.hpp"
#include "muxb.hpp"
#include "shift.hpp"
#include "muxt.hpp"
#include "muxb3.hpp"

SC_MODULE(Cpu) {

	static const unsigned DATA_WIDTH = 16;	 
	static const unsigned TEST_NUM= 5;
	static const unsigned DEPTH = 1 << 8; //memoria
	static const unsigned DATA_OPCODE = 3;
	static const unsigned DATA_ALU = 6;
	static const unsigned DATA_MUXPC = 2;
	static const unsigned DATA_MUXTGT = 2;

	//porte CPU:
	sc_in<bool> ck;
	sc_in<bool> reset;

	//canali per il registro
	sc_signal<sc_lv<DATA_WIDTH> > reg_din;
	sc_signal<bool> reg_ck;
	sc_signal<bool> reg_reset;
	sc_signal<sc_lv<DATA_WIDTH> > reg_dout; 
		
	//canali per Adder
	sc_signal<sc_lv<DATA_WIDTH> > adder_op1;
	sc_signal<sc_lv<DATA_WIDTH> > adder_op2;
	sc_signal<sc_lv<DATA_WIDTH> > adder_result;	
	sc_signal<sc_lv<DATA_WIDTH> > adder2_result;	
	
	//canali per la memoria
	sc_signal<sc_lv<DATA_WIDTH> > mem_add;
	sc_signal<sc_lv<DATA_WIDTH> > mem_din;
	sc_signal<sc_lv<DATA_WIDTH> > mem_dout;
	sc_signal<sc_lv<DATA_WIDTH> > dmem_add;
	sc_signal<sc_lv<DATA_WIDTH> > dmem_din;
	sc_signal<sc_lv<DATA_WIDTH> > dmem_dout;	
	sc_signal<bool> mem_cs; //chip select
	sc_signal<bool> mem_we; //write enable
	sc_signal<bool> mem_oe; //output enable	
	sc_signal<bool> dmem_cs; //chip select
	sc_signal<bool> dmem_oe; //output enable	
	sc_signal<bool> dmem_we; //write enable
	sc_signal<unsigned> mem_file;
	sc_signal<unsigned> dmem_file;
		
	//canali per Splitter
  sc_signal<sc_lv<DATA_WIDTH> > split_din;
  sc_signal<sc_lv<3> > split_opcode;
  sc_signal<sc_lv<3> > split_regA;
  sc_signal<sc_lv<3> > split_regB;
  sc_signal<sc_lv<3> > split_regC;
  sc_signal<sc_lv<7> > split_immediate7;
  sc_signal<sc_lv<10> > split_immediate10;	

	//canali per Control
	sc_signal<bool> control_unEq;
	sc_signal<sc_lv<DATA_OPCODE> > control_opcode;
	sc_signal<sc_lv<DATA_ALU> > control_funcAlu;	
	sc_signal<sc_lv<DATA_MUXPC> > control_muxPc;
	sc_signal<sc_lv<DATA_MUXTGT> > control_muxTgt;
	sc_signal<bool> control_muxAlu1;
	sc_signal<bool> control_muxAlu2;	
	sc_signal<bool> control_weDmem;
	sc_signal<bool> control_weRf;
	sc_signal<bool> control_muxRf;

	//canali per il register file
	sc_signal<sc_lv<DATA_OPCODE> > ref_add1;
	sc_signal<sc_lv<DATA_OPCODE> > ref_add2;
	sc_signal<sc_lv<DATA_OPCODE> > ref_add3;
	sc_signal<sc_lv<DATA_WIDTH> > ref_dataw;
	sc_signal<sc_lv<DATA_WIDTH> > ref_dout1;
	sc_signal<sc_lv<DATA_WIDTH> > ref_dout2;
	sc_signal<bool> ref_we; //write enable dataw

	//canali per l'ALU
	sc_signal<sc_lv<DATA_WIDTH> > alu_op1;
	sc_signal<sc_lv<DATA_WIDTH> > alu_op2;
	sc_signal<sc_lv<DATA_ALU> > alu_func;
	sc_signal<sc_lv<DATA_WIDTH> > alu_result;
	sc_signal<bool> alu_zero;

	//canali per Extend
	sc_signal<sc_lv<7> > extend_din;
	sc_signal<sc_lv<DATA_WIDTH> > extend_dout;
	
	//canali per il multiplexer 2 ingressi
	sc_signal<sc_lv<DATA_WIDTH> > muxb_din[2];
	sc_signal<bool> muxb_dctr;
	
	sc_signal<sc_lv<DATA_WIDTH> > muxb_dout;
	sc_signal<sc_lv<DATA_WIDTH> > muxb2_dout;
	
	sc_signal<sc_lv<3> > muxb3_din[2];
	sc_signal<sc_lv<3> > muxb3_dout;
	
	//canali per Shift
	sc_signal<sc_lv<10> > shift_din;
	sc_signal<unsigned> shift_val;
	sc_signal<sc_lv<DATA_WIDTH> > shift_dout;

	//canali per il multiplexer 3 ingressi
	sc_signal<sc_lv<DATA_WIDTH> > muxt_din[3];
	sc_signal<sc_lv<2> > muxt_dctr;
	sc_signal<sc_lv<DATA_WIDTH> > muxt_dout;
	sc_signal<sc_lv<DATA_WIDTH> > muxt2_dout;
	
	sc_signal<sc_lv<DATA_WIDTH> > alu_result_condiviso;
							
	Register prog_count; //REGISTRO1
	Adder adder1 ; //ADDER
	Memory inst_mem; //INSTRUCTION MEMORY
	Splitter split;
	Control control1; //CONTROL UNIT	
	Register_file ref1; //REGISTER FILE
	//Alu alu1; //ALU
	ALU_RTL alu1; //ALU versione RTL
	Extend extend1 ; //EXTEND SIGN
	Muxb muxb; //MULTIPLEXER 2in
	Shift shift1; //SHIFT VECTOR
	Muxb muxb2; //MULTIPLEXER 2in		
	Memory data_mem; //DATA MEMORY	
	Muxt muxt; //MULTIPLEXER 3in	
	Muxb3 muxb3;	//MULTIPLEXER 2in
	Adder adder2; //ADDER Beq Jalr
	Muxt muxt2; //MULTIPLEXER 3in PC
	
Cpu(sc_module_name cpu): sc_module(cpu), prog_count("prog_count"), adder1("adder1"), inst_mem("Instruction_memory"), split("splitter"), control1("control1"), ref1("Register_file"), alu1("ALU"), extend1("extend"), muxb("multiplexer"), shift1("schifter"), muxb2("multiplexer2"), data_mem("data_memory"), muxt("multiplexer3"),muxb3("multiplexer4"), adder2("adder2"), muxt2("multiplexer5")
{

		//Cpu:

		//Adder +1:
		SC_THREAD(init_values_adder); 
			adder1.op1(prog_count.dout); 
			adder1.op2(adder_op2);          //sempre 1
			adder1.result(adder_result);
			SC_THREAD(observer_thread_adder);
		  	sensitive << adder1.result;		
					
		//Program counter:
			//SC_THREAD(clock_thread);
			SC_THREAD(init_values_register);
			prog_count.din(muxt2.dout); 
			prog_count.dout(reg_dout);
			prog_count.ck(this->ck);
			prog_count.reset(this->reset);	
			SC_THREAD(observer_thread_register);
				sensitive << prog_count.ck << prog_count.dout;
				
		//Instruction memory:
			SC_THREAD(init_values_memory);
			inst_mem.add(prog_count.dout);
			inst_mem.din(mem_din);
			inst_mem.dout(mem_dout);
			inst_mem.cs(mem_cs);		
			inst_mem.we(mem_we);
			inst_mem.oe(mem_oe);
			inst_mem.file(mem_file);	
			SC_THREAD(observer_thread_memory);	
				sensitive << inst_mem.dout;		
		
		//Splitter:
			SC_THREAD(init_values_splitter);
			split.din(inst_mem.dout);
			split.opcode(split_opcode);
			split.regA(split_regA);
			split.regB(split_regB);
			split.regC(split_regC);
			split.immediate7(split_immediate7);
			split.immediate10(split_immediate10);
			SC_THREAD(observer_thread_splitter);	
				sensitive << split.opcode << split.regA << split.regB << split.regC << split.immediate7 << split.immediate10;					
		
		//Control:
			SC_THREAD(init_values_control);
			control1.unEq(alu1.zero);
			control1.opcode(split.opcode);
			control1.funcAlu(control_funcAlu);
			control1.muxPc(control_muxPc);
			control1.muxTgt(control_muxTgt);
			control1.muxAlu1(control_muxAlu1);
			control1.muxAlu2(control_muxAlu2);
			control1.weDmem(control_weDmem);
			control1.weRf(control_weRf);
			control1.muxRf(control_muxRf);		
			SC_THREAD(observer_thread_control);	
				sensitive << control1.unEq << control1.funcAlu << control1.muxPc << control1.muxTgt << control1.muxAlu1 << control1.muxAlu2 << control1.weDmem << control1.weRf << control1.muxRf;

		//Register file:
			SC_THREAD(init_values_register_file);
			ref1.add1(split.regB);
			ref1.add2(muxb3.dout);
			ref1.add3(split.regA);		
			ref1.dataw(muxt.dout);
			ref1.dout1(ref_dout1);
			ref1.dout2(ref_dout2);
			ref1.we(control1.weRf);
			SC_THREAD(observer_thread_register_file);	
				sensitive << ref1.dout1 << ref1.dout2;					
 		
 		//Alu:
			SC_THREAD(init_values_alu);
			alu1.op1(muxb2.dout);
			alu1.op2(muxb.dout);
			alu1.func(control1.funcAlu);
			alu1.result(alu_result_condiviso);
			alu1.zero(alu_zero);		
			SC_THREAD(observer_thread_alu);	
				sensitive << alu1.result << alu1.zero;

		//Extend:
			SC_THREAD(init_values_extend);
			extend1.din(split.immediate7);
			extend1.dout(extend_dout);		
			SC_THREAD(observer_thread_extend);
				sensitive << extend1.dout;
		
	  //Mux1 2in:
			SC_THREAD(init_values_muxb);
			muxb.dctr(control1.muxAlu2);
			muxb.dout(muxb_dout);
			muxb.din[0](ref1.dout2);
			muxb.din[1](extend1.dout);
			SC_THREAD(observer_thread_muxb);
				sensitive << muxb.dout;
					
		//Shift:
			SC_THREAD(init_values_shift);
			shift1.din(split.immediate10);
			shift1.dout(shift_dout);
			shift1.val(shift_val);		
			SC_THREAD(observer_thread_shift);
				sensitive << shift1.dout;
		
		//Mux 2 
			SC_THREAD(init_values_muxb2);
			muxb2.dctr(control1.muxAlu1);
			muxb2.dout(muxb2_dout);
			muxb2.din[0](ref1.dout1);
			muxb2.din[1](shift1.dout);
			SC_THREAD(observer_thread_muxb2);
				sensitive << muxb2.dout;	
		
		//Data memory:
			SC_THREAD(init_values_data_memory);
			data_mem.add(alu1.result);
			data_mem.din(ref1.dout2);
			data_mem.dout(dmem_dout);
			data_mem.cs(dmem_cs);		
			data_mem.we(control1.weDmem);
			data_mem.oe(dmem_oe);
			data_mem.file(dmem_file);	
			SC_THREAD(observer_thread_data_memory);	
				sensitive << data_mem.dout;		
					
	  //Mux3 3in:
			SC_THREAD(init_values_muxt);
			muxt.dctr(control1.muxTgt);
			muxt.dout(muxt_dout);
			muxt.din[0](alu1.result);
			muxt.din[1](data_mem.dout);
			muxt.din[2](adder1.result);
			SC_THREAD(observer_thread_muxt);
				sensitive << muxt.dout;
		
		//Mux 4 
			SC_THREAD(init_values_muxb3);
			muxb3.dctr(control1.muxRf);
			muxb3.dout(muxb3_dout);
			muxb3.din[0](split.regC);
			muxb3.din[1](split.regA);
			SC_THREAD(observer_thread_muxb3);
				sensitive << muxb3.dout;	
					
 		//Adder BEQ JALR:
			SC_THREAD(init_values_adder2); 
			adder2.op1(adder1.result); 
			adder2.op2(extend1.dout);          
			adder2.result(adder2_result);
			SC_THREAD(observer_thread_adder2);
		  	sensitive << adder2.result;		

	  //Mux5 3in:
			SC_THREAD(init_values_muxt2);
			muxt2.dctr(control1.muxPc);
			muxt2.dout(muxt2_dout);
			muxt2.din[0](adder1.result);
			muxt2.din[1](adder2.result);
			muxt2.din[2](alu1.result);
			SC_THREAD(observer_thread_muxt2);
				sensitive << muxt2.dout;	
  } 
	SC_HAS_PROCESS(Cpu);
	
  //Registro:
	void clock_thread();
	void init_values_register();

	
	//Adder:
	void init_values_adder();
	void init_values_adder2();
	
	//Memoria:
	void init_values_memory();
	void init_values_data_memory();
	
  //Splitter:
  void init_values_splitter();

  //Control:
  void init_values_control();

  //Register file:
  void init_values_register_file();

	//Alu:
	void init_values_alu();

	//Mux:
  void init_values_muxb();
  void init_values_muxb2();
  void init_values_muxt();
  void init_values_muxb3();
  void init_values_muxt2();
    
  //Shift:
  void init_values_shift();  
    
	//Cpu:
	void observer_thread_adder();
	void observer_thread_register();
	void observer_thread_memory();
	void observer_thread_splitter();
	void observer_thread_control();
	void observer_thread_register_file();
	void observer_thread_alu();	
	void observer_thread_extend();
	void observer_thread_muxb();
	void observer_thread_muxb2();
	void observer_thread_shift();
	void observer_thread_data_memory();	
	void observer_thread_muxt();	
	void observer_thread_muxb3();
	void observer_thread_adder2();
	void observer_thread_muxt2();			
	void init_values();

	//Extend:
  void init_values_extend();
  
};

#endif
