#include <systemc.h>
#include "cpu.hpp"

#include <fstream>
#include <string>
#include <iostream>

using namespace std;
using namespace sc_core;

SC_MODULE(TestBench) 
{
 	public:
  sc_signal<bool> ck;
  sc_signal<bool> reset;
  
  Cpu cpu1;
  
  SC_CTOR(TestBench) : cpu1("cpu1")
  {
    SC_THREAD(clock_thread);
    SC_THREAD(reset_thread);
    SC_THREAD(watcher_thread);
      sensitive << ck;

    cpu1.ck(this->ck);
    cpu1.reset(this->reset);
    
  }

	int check_instructionSW() {

		string line;
		ifstream myfile ("memory2_end.txt");
		int i=0;
		if (myfile.is_open())
		{
		  while ( getline (myfile,line) )
		  {
		    
		    if (i==4) {
		    	if (line.compare("0000000000001010") !=0) {
		    		cout << line << '\n';
		    		myfile.close();
		    		return 1;
		    		}
		  	}
		    if (i==12) {
		    	if (line.compare("0000000000011111") !=0) {
		    		cout << line << '\n';
		    		myfile.close();
		    		return 1;
		    		}
		  	}    	
		  	i++;
			}
			myfile.close();
		}
		else {
			cout << "Unable to open file\n"; 
			return 1;
		}
	 	return 0;
	}

  void watcher_thread()
  {
    while(true)
    {
      wait();
      if(ck.read() && reset.read())
      {
          cout << "Time " << sc_time_stamp() << endl;
      }
    }
  }

 private:

  void reset_thread()
  {
  
  		int test_reset = 1;
			reset.write(test_reset);
			wait(1,SC_NS);
	
			test_reset = 0;
			reset.write(test_reset);
			wait(SC_ZERO_TIME);
	
  }

	void clock_thread() {
		bool value = false;
		while(true) {
			ck.write(value);
			value = !value;
			wait(10,SC_NS);
		}
	}

};

int sc_main(int argc, char* argv[]) {

	//carico istruzione SW in instrucition memory
	int i=0;
  ofstream out;
  out.open ("memory1.txt");				  // opcode(3)+regA(3)+reg(3)+(4)+regC(3)
	out << "1010010100000001";				// SW regA[1],regB[2],imm7 -> data_mem[regB[2]+imm7]=regA[1]
	out << "\n";
	out << "1011000010000010";				// SW regA[4],regB[1],imm7 -> data_mem[regB[1]+imm7]=regA[4]
	out << "\n";
	
	do {
		out << "0000000000000000";				
		out << "\n";
		i++;
	} while (i < 60);	
  out.close();	

	//carico valori di test nel register file
	out.open("register_file_start.txt");

	out << "0000000000000000";
	out << "\n";
	out << "0000000000001010";
	out << "\n";
	out << "0000000000000011";
	out << "\n";
	out << "0000000000000000";
	out << "\n";
	out << "0000000000011111";
	out << "\n";
	out << "0000000000000000";
	out << "\n";
	out << "0000000000000000";
	out << "\n";
	out << "0000000000000000";
  out.close();
  
	//carico valori di test nella data memory
	out.open("memory2.txt"); //la memoria parte dall'indice 1 (non zero come gli altri file)

	out << "0000000000000000";
	out << "\n";
	out << "0000000000000000";
	out << "\n";
	out << "0000000000000000";
	out << "\n";
	out << "0000000000000000";
	out << "\n";
	out << "0000000000000000";
	out << "\n";
	out << "0000000000000000";
	out << "\n";
	out << "0000000000000000";
	out << "\n";
	out << "0000000000000000";
  out.close();  		

	TestBench testbench("testbench");
	sc_start(200,SC_NS);
	
  return testbench.check_instructionSW();
}
