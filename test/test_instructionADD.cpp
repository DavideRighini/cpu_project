#include <systemc.h>
#include "cpu.hpp"

#include <fstream>
#include <string>
#include <iostream>

using namespace std;
using namespace sc_core;

SC_MODULE(TestBench) 
{
 	public:
  sc_signal<bool> ck;
  sc_signal<bool> reset;
  
  Cpu cpu1;
  
  SC_CTOR(TestBench) : cpu1("cpu1")
  {
    SC_THREAD(clock_thread);
    SC_THREAD(reset_thread);
    SC_THREAD(watcher_thread);
      sensitive << ck;

    cpu1.ck(this->ck);
    cpu1.reset(this->reset);
    
  }

	int check_instructionADD() {

		string line;
		ifstream myfile ("register_file_end.txt");
		int i=0;
		if (myfile.is_open())
		{
		  while ( getline (myfile,line) )
		  {
		    cout << line << '\n';
		    if (i==6) {
		    	if (line.compare("0000000000001011") !=0) {
		    		myfile.close();
		    		return 1;
		    		}
		  	}
		  	if (i==7) {
		    	if (line.compare("0000000000010110") !=0) {
		    		myfile.close();
		    		return 1;
		    		}
		  	}
		  	i++;
			}
			myfile.close();
		}	else {
			cout << "Unable to open file\n"; 
			return 1;
		}
	 	return 0;
	}

  void watcher_thread()
  {
    while(true)
    {
      wait();
      if(ck.read() && reset.read())
      {
          cout << "Time " << sc_time_stamp() << endl;
      }
    }
  }

 private:

  void reset_thread()
  {
  
  		int test_reset = 1;
			reset.write(test_reset);
			wait(1,SC_NS);
	
			test_reset = 0;
			reset.write(test_reset);
			wait(SC_ZERO_TIME);
	
  }

	void clock_thread() {
		bool value = false;
		while(true) {
			ck.write(value);
			value = !value;
			wait(10,SC_NS);
		}
	}

};

      
int sc_main(int argc, char* argv[]) {
	
	//carico istruzione ADD in instrucition memory
	int i=0;
  ofstream out;
  out.open ("memory1.txt");				  // opcode(3)+regA(3)+reg(3)+(4)+regC(3)
	out << "0001010100000001";				// ADD regA[5],regB[2],regC[1] -> regA[5]=regB[2]+regC[1] 8 = 5+3
	out << "\n";
	out << "0001101010000001";				// ADD regA[6],regB[5],regC[1] -> regA[6]=regB[5]+regC[1] 11 = 8+3
	out << "\n";
	out << "0001111100000000";				// ADD regA[7],regB[6],regC[0] -> regA[7]=regB[6]+regC[0] 11 = 11+0
	out << "\n";	
	out << "0001111110000111";				// ADD regA[7],regB[7],regC[7] -> regA[7]=regB[7]+regC[7] 22 = 11 +11
	out << "\n";	
	
	do {
		out << "0000000000000000";				
		out << "\n";
		i++;
	} while (i < 60);	
  out.close();	

	//carico valori di test nel register file
	out.open("register_file_start.txt");

	out << "0000000000000000";
	out << "\n";
	out << "0000000000000011";
	out << "\n";
	out << "0000000000000101";
	out << "\n";
	out << "0000000000000000";
	out << "\n";
	out << "0000000000000000";
	out << "\n";
	out << "0000000000000000";
	out << "\n";
	out << "0000000000000000";
	out << "\n";
	out << "0000000000000000";
  out.close();	
  	
	TestBench testbench("testbench");
	sc_report_handler::set_actions (SC_ID_VECTOR_CONTAINS_LOGIC_VALUE_,SC_DO_NOTHING);
	sc_start(200,SC_NS);
	
  return testbench.check_instructionADD();
}
