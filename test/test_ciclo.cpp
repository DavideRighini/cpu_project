#include <systemc.h>
#include "cpu.hpp"

#include <fstream>
#include <string>
#include <iostream>

using namespace std;
using namespace sc_core;

SC_MODULE(TestBench) 
{
 	public:
  sc_signal<bool> ck;
  sc_signal<bool> reset;
  
  Cpu cpu1;
  
  SC_CTOR(TestBench) : cpu1("cpu1")
  {
    SC_THREAD(clock_thread);
    SC_THREAD(reset_thread);
    SC_THREAD(watcher_thread);
      sensitive << ck;

    cpu1.ck(this->ck);
    cpu1.reset(this->reset);
    
  }

	int check_ciclo() {

		string line;
		ifstream myfile ("memory2_end.txt");
		int i=0;
		if (myfile.is_open())
		{
		  while ( getline (myfile,line) )
		  {
		    cout << line << '\n';
		    if (i==0) {
		    	if (line.compare("0000000000100011") !=0) {
		    		myfile.close();
		    		return 1;
		    		}
		  	}
		  	i++;
			}
			myfile.close();
		}	else {
			cout << "Unable to open file\n"; 
			return 1;
		}
	 	return 0;
	}

  void watcher_thread()
  {
    while(true)
    {
      wait();
      if(ck.read() && reset.read())
      {
          cout << "Time " << sc_time_stamp() << endl;
      }
    }
  }

 private:

  void reset_thread()
  {  
  		int test_reset = 1;
			reset.write(test_reset);
			wait(1,SC_NS);
	
			test_reset = 0;
			reset.write(test_reset);
			wait(SC_ZERO_TIME);	
  }

	void clock_thread() {
		bool value = false;
		while(true) {
			ck.write(value);
			value = !value;
			wait(10,SC_NS);
		}
	}

};


int sc_main(int argc, char* argv[]) {

	//carico set istruzioni nell'instruction memory
	int i=0;
  ofstream out;
  out.open ("memory1.txt");				  // opcode(3)+regA(3)+reg(3)+(4)+regC(3)
  
	out << "0000100000000000";				// add r(2) = r(0) + r(0)	
	out << "\n";
	out << "0010110100000101";				// addi r(3) = r(2) + 5
	out << "\n";
	out << "0011000100000111";				// addi r(4) = r(2) + 7
	out << "\n";
	out << "0010010100000001";				// addi r(1) = r(2) + 1
	out << "\n";
	out << "0001011010000011";				// add r(5) = r(5) + r(3)
	out << "\n";
	out << "0000100100000001";				// add r(2) = r(2) + r(1)
	out << "\n";
	out << "1101000100000011";				// beq if(r(4) == r(2)) PC= PC+1+ 3
	out << "\n";
	out << "0111100000000000";				// lui r(6)=0
	out << "\n";	
	out << "0011101100000101";				// addi r(6)=r(6)+5
	out << "\n";
	out << "1110001100000000";				// jalr jump to r(6)
	out << "\n";
	out << "1011010000000000";				// sw mem[r(0) + 0] = r(5)
	out << "\n";
				
	do {
		out << "0000000000000000";				
		out << "\n";
		i++;
	} while (i < 60);	
  out.close();	

	//carico valori di test nel register file
	out.open("register_file_start.txt");

	out << "0000000000000000";
	out << "\n";
	out << "0000000000000000";
	out << "\n";
	out << "0000000000000000";
	out << "\n";
	out << "0000000000000000";
	out << "\n";
	out << "0000000000000000";
	out << "\n";
	out << "0000000000000000";
	out << "\n";
	out << "0000000000000000";
	out << "\n";
	out << "0000000000000000";
  out.close();	
	
	TestBench testbench("testbench");
	sc_start(1000,SC_NS); 
	
  return testbench.check_ciclo();
}
