#include <systemc.h>
#include "cpu.hpp"

#include <iostream>
#include <fstream>
#include <string>

using namespace std;
   
void Cpu::init_values_register() {

}

void Cpu::observer_thread_register() {
	while(true) {
		wait();
		int value = prog_count.dout->read().to_uint();
		bool value1 = prog_count.reset->read();
		bool value2 = prog_count.ck->read();
		cout << "observer_thread: at " << sc_time_stamp() << " reg out: " << value << " ck: " << value2 << " reset: " << value1 << endl;
	}
}

void Cpu::init_values_adder() {
	adder_op2.write(1);	//devo sommare 1
	wait(SC_ZERO_TIME);
}

void Cpu::observer_thread_adder(){
	while(true) {
	  wait();
	  int value = adder1.result->read().to_uint();
	  cout << "observer_thread: at " << sc_time_stamp() << " adder out: " << value << endl;
	} 
}

void Cpu::init_values_memory() {
	//modalità lettura
	mem_cs.write(1);
	mem_we.write(0);
	mem_oe.write(1);
	mem_file.write(1);
	wait(1,SC_NS);
}

void Cpu::observer_thread_memory() {
	while(true) {
	  wait();
	  int value = inst_mem.dout->read().to_int();
	  cout << "\n\nobserver_thread: at " << sc_time_stamp() << " instruction memory out: " << inst_mem.dout->read() << "\n\n";
	} 
}

void Cpu::init_values_splitter() {

}

void Cpu::observer_thread_splitter() {
	while(true) {
		wait();
		cout << "observer_thread: at " << sc_time_stamp() << " splitter out: " << split.din->read() << endl;
		cout << "observer_thread: at " << sc_time_stamp() << " splitter out: " << split.opcode->read() << " " << split.regA->read() << " " << split.regB->read() << " " <<  split.regC->read() << " imm7: " << split.immediate7->read() << " imm10: " << split.immediate10->read() << "\n\n";
	} 	
}

void Cpu::init_values_control() {

}

void Cpu::observer_thread_control() {
	while(true) {
		wait();
		unsigned value = control1.opcode->read().to_uint();
		cout << "\n\nobserver_thread: at " << sc_time_stamp() << " control opcode: " << value << endl;
		cout << "observer_thread: at " << sc_time_stamp() << " control alu_func: " << control1.funcAlu->read().to_uint() << "\n";
		cout << "observer_thread: at " << sc_time_stamp() << " control weRf: " << control1.weRf->read() << "\n";
		cout << "observer_thread: at " << sc_time_stamp() << " control muxAlu1: " << control1.muxAlu1->read() << "\n";
		cout << "observer_thread: at " << sc_time_stamp() << " control muxAlu2: " << control1.muxAlu2->read() << "\n";
		cout << "observer_thread: at " << sc_time_stamp() << " control muxTgt: " << control1.muxTgt->read().to_uint() << "\n";
		cout << "observer_thread: at " << sc_time_stamp() << " control muxRf: " << control1.muxRf->read() << "\n";
		cout << "observer_thread: at " << sc_time_stamp() << " control muxPc: " << control1.muxPc->read() << "\n";
		cout << "observer_thread: at " << sc_time_stamp() << " control weDmem: " << control1.weDmem->read() << "\n\n";
	} 
}

void Cpu::init_values_register_file() {

}

void Cpu::observer_thread_register_file() {
	while(true) {
	  wait();
	  int out1 = ref1.dout1->read().to_int();
	  int out2 = ref1.dout2->read().to_int();
	  cout << "observer_thread: at " << sc_time_stamp() << " reg file dataw: " << ref1.dataw << endl;
	  cout << "observer_thread: at " << sc_time_stamp() << " reg file out1: " << out1 << endl;
	  cout << "observer_thread: at " << sc_time_stamp() << " reg file out2: " << out2 << "\n\n";
	} 
}

void Cpu::init_values_alu() {

}

void Cpu::observer_thread_alu() {
	while(true) {
		wait();
		int value = alu1.result->read().to_int();
		cout << "\nobserver_thread: at " << sc_time_stamp() << " alu out: " << value << endl;
		cout << "observer_thread: at " << sc_time_stamp() << " alu in1: " << alu1.op1->read() << " alu in2: " << alu1.op2->read() << endl;
		cout << "observer_thread: at " << sc_time_stamp() << " alu zero: " << alu1.zero << endl;
		cout << "observer_thread: at " << sc_time_stamp() << " alu func: " << alu1.func->read() << "\n\n";
	} 
}

void Cpu::init_values_shift() {
  shift_val.write(6); //devo shiftare il vettore di 6 posti
	wait(SC_ZERO_TIME);	
}

void Cpu::observer_thread_shift() {
	while(true) {
	  wait();
	  int value = shift1.dout->read().to_int();
	  cout << "observer_thread: at " << sc_time_stamp() << " shift out: " << shift1.dout->read() << endl;
	} 
}

void Cpu::init_values_muxb2() {

}

void Cpu::observer_thread_muxb2() {
	while(true) {
	  wait();
	  int value = muxb2.dout->read().to_int();
	  cout << "observer_thread: at " << sc_time_stamp() << " muxb2 out: " << value << endl;
	} 
}

void Cpu::init_values_data_memory() {
	//modalità lettura
	dmem_cs.write(1);
	dmem_we.write(0);
	dmem_oe.write(1);
	dmem_file.write(2);
	wait(1,SC_NS);
}

void Cpu::observer_thread_data_memory() {
	while(true) {
	  wait();
	  int value = data_mem.dout->read().to_int();
	  cout << "observer_thread: at " << sc_time_stamp() << " data memory out: " << data_mem.dout->read() << endl;
	} 
}

void Cpu::init_values_muxt() {

}

void Cpu::observer_thread_muxt() {
	while(true) {
	  wait();
	  int value = muxt.dout->read().to_int();
	  cout << "observer_thread: at " << sc_time_stamp() << " muxt out: " << value << endl;			
	} 		
}

void Cpu::init_values_muxb3() {

}

void Cpu::observer_thread_muxb3() {
	while(true) {
	  wait();
	  unsigned value = muxb3.dout->read().to_uint();
	  cout << "observer_thread: at " << sc_time_stamp() << " muxb3 out: " << value << endl;
	} 
}

void Cpu::init_values_adder2() {

}

void Cpu::observer_thread_adder2() {
	while(true) {
	  wait();
	  int value = adder2.result->read().to_int();
	  cout << "observer_thread: at " << sc_time_stamp() << " adder2 out: " << value << endl;
	} 
}

void Cpu::init_values_muxt2() {

}

void Cpu::observer_thread_muxt2() {
	while(true) {
	  wait();
	  int value = muxt2.dout->read().to_int();
	  cout << "observer_thread: at " << sc_time_stamp() << " muxt2 out: " << value << endl;
	} 		
}

void Cpu::init_values_extend() {

}

void Cpu::observer_thread_extend() {
	while(true) {
	  wait();
	  int value = extend1.dout->read().to_int();
	  cout << "observer_thread: at " << sc_time_stamp() << " extend out: " << extend1.dout->read() << endl;
	} 
}

void Cpu::init_values_muxb() {

}

void Cpu::observer_thread_muxb() {
	while(true) {
	  wait();
	  int value = muxb.dout->read().to_int();
	  cout << "observer_thread: at " << sc_time_stamp() << " muxb out: " << value << endl;
	} 
}
